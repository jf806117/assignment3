# -*- coding: utf-8 -*-
"""
Created on Mon Oct 18 10:18:23 2021

@author: jf806117
"""
import numpy as np
import matplotlib.pyplot as plt

# Dictionary defintions
pa = 1e5             # mean atmospheric pressure (Pa)
pb = 200               # pressure variations (Pa)
L = 2.4e6          # length scale of variations (m)
f = 1e-4             # coriolis parameter (rad/s)
r = 1                  # density of air (kg/m^3)
ymin = 0               # y domain start (m)
ymax = 1e6           # y domain end (m)

# Size and resolution of the domain
N = 10                       # number of intervals
dy = (ymax - ymin)/N          # size of intervals (m)

# Give the domain a dimension y
y = np.linspace(ymin, ymax, N+2)

# Pressure across the domain
p = pa + pb*np.cos(y*np.pi/L)

# Pressure gradient across the domain
dpdy = -pb*(np.pi/L)*np.sin(y*np.pi/L)

# Geostrophic wind across the y domain
geowind = (-1/r*f)*dpdy

# Numerical wind across the domain
numWindPlusH = np.zeros_like(y)                      # initial array of zeros
for i in range(1,N):                            # wind across domain
    numWindPlusH[i] =(-1/r*f)*(p[i-1]-27*p[i]+27*p[i+1]-p[i+2])/24*dy

# Plot geostrophic and numerical wind over the domain
plt.plot(y/1000, geowind, label="geostrophic")
plt.plot(y/1000, numWindPlusH, label="numerical")
plt.legend()
plt.xlabel("y (km)")
plt.ylabel("wind speed (m/s)")
plt.tight_layout()
plt.show()

#Plotting the Errors
plt.plot(y/1000, numWindPlusH-geowind, '*k--',
         label ='Two-point Differences')
plt.legend()
plt.xlabel('y(km)')
plt.ylabel('u_error (m/s)')
plt.tight_layout()
plt.show()


